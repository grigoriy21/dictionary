<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = false;
    protected $table = 'posts';
    protected $fillable = ['word', 'translation'];

    public static function scopePagQuery($query, $char = null)
    {
        if ($char) {
            $char = preg_replace('#[^a-z]#i', '', $char);
            return $query->where('word', 'like', "{$char}%");
        } else {
            return $query->orderBy('word');
        }
    }
}

