<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Request $request)
    {
//      dd($request);
      $result = Post::PagQuery($request->get('char'))->get();
      return view('posts.index', compact( 'result'));
    }

//    public function show(Post $post)
//    {
//        return view('posts.show', compact('post'));
//    }
//
//    public function create()
//    {
//        return view('posts.create');
//    }

    public function store(Request $request)
    {

//        $this->validate(request(), [
//            'word' => 'required',
//            'translation' => 'required',
//
//        ]);
//        dd($request->all());
        Post::create($request->all());

        return redirect('/');
    }

}